const Joi = require('@hapi/joi');

const id = Joi.number().integer();
const name = Joi.string().max(80);
const price = Joi.number().integer().min(100);
const description = Joi.string().min(10)
const image = Joi.string().uri();
const categoryId = Joi.number().integer();

const CreateProductSchema = Joi.object({
    name: name.required(),
    price: price.required(),
    description: description.required(),
    image: image.required(),
    categoryId: categoryId.required()
});

const UpdateProductSchema = Joi.object({
    name: name,
    price: price,
    description: description,
    image: image,
    categoryId
});

const ProductIdSchema = Joi.object({
    id: id
});

const QueryProductSchema = Joi.object({
    limit: Joi.number().integer(),
    offset: Joi.number().integer(),
    price,
    price_min: price,
    price_max: price.when('price_min', {
        is: price,
        then: Joi.required()
    })
});

module.exports = { CreateProductSchema, UpdateProductSchema, ProductIdSchema, QueryProductSchema };