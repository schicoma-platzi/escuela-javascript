import ALL_ADS from "./AllAds";

class Ads {
    private static instance: Ads;
    private ads: Array<Ad>;

    private constructor() {
        this.initAds();
    }

    public static getInstance(): Ads {
        if (!this.instance) {
            this.instance = new Ads();
        }

        return this.instance;
    }

    private initAds() {
        this.ads = [...ALL_ADS];
    }

    getAd() {
        if (!this.ads.length) {
            this.initAds();
        }

        return this.ads.pop();
    }
}

export default Ads;