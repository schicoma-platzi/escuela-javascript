require('dotenv').config();

const config = {
    env: process.env.NODE_ENV || 'development',
    isProduction: process.env.NODE_ENV === 'production',
    port: process.env.PORT || 3131,
    dbDialect: process.env.DB_DIALECT,
    dbUser: process.env.DB_USER,
    dbPassword: process.env.DB_PASS,
    dbName: process.env.DB_NAME,
    dbPort: process.env.DB_PORT,
    dbHost: process.env.DB_HOST,
    dbUrl: process.env.DATABASE_URL
};

module.exports = { config };