# Curso de Backend con Node.js: Base de Datos con PostgreSQL

## PostgreSQL

### node-postgres

Se usará la librería ```node-postgress``` ([Ver documentación](https://node-postgres.com/)) para realizar la conexión hacia la base de datos.

### Queries utilizados en el proyecto

```sql
CREATE TABLE task (
	id serial PRIMARY KEY,
	title VARCHAR ( 250 ) NOT NULL,
	completed boolean DEFAULT false
);
```

## Sequelize

### ORM

Este ORM para Node.js soporta diferentes base de datos, entre ellas tenemos a: mysql, postgresql, mariadb, etc.
Ejecutar el comando para instalar el orm (y los drivers necesarios): ```npm install --save sequelize pg pg-hstore mysql2```

### Migraciones

Es la forma como sequelize mantiene un seguimiento de los cambios que se hacen a las entidades de nuestra base de datos.

## Deploy to Heroku

```heroku create```
```git subtree push --prefix curso_node_backend_postgresql/my-store heroku main```

```heroku plugins:install heroku-repo &&  heroku repo:reset -a appname``` (En caso haya conflictos con git subtree)

### Addons

```heroku addons:create heroku-postgresql:hobby-dev -a <appName>```
```heroku pg:info```

### Migraciones en posgtresql (Heroku)

```heroku run npm run migrations:run```