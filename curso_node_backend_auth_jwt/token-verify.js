const jwt = require('jsonwebtoken');

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInJvbGUiOiJjdXN0b21lciIsImlhdCI6MTY0MTMyNTMxMX0.m2WKgY7NVmEm0-mc_sS9BJDRM3dcQuZd9k5WlmRUQkM';
const okSecret = 'my-secret-key';
const badSecret = 'my-secret';

function verifyToken(token, secret) {
    return jwt.verify(token, secret)
}

console.log(verifyToken(token, okSecret)); // { sub: 1, role: 'customer', iat: 1641325311 }
console.log(verifyToken(token, badSecret)); // exception