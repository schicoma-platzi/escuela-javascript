const { Sequelize } = require('sequelize');

const { config } = require('../config/config');
const setupModels = require('../db/models');

const DB_DIALECT = encodeURIComponent(config.dbDialect);
let uri;

const options = {
    dialect: DB_DIALECT
};

if (config.isProduction) {
    uri = config.dbUrl;

    options.logging = false;
    options.dialectOptions = {
        ssl: {
            rejectUnauthorized: false
        }
    };
} else {
    const USER = encodeURIComponent(config.dbUser);
    const PASS = encodeURIComponent(config.dbPassword);
    uri = `${DB_DIALECT}://${USER}:${PASS}@${config.dbHost}:${config.dbPort}/${config.dbName}`;
}

const sequelize = new Sequelize(uri, options);

// sequelize inicializará los modelos creados en models/index.js
setupModels(sequelize);

// creará la estructura en la bd (deshabilitar para prod, usar migraciones es mejor opción)
// sequelize.sync();

module.exports = sequelize;