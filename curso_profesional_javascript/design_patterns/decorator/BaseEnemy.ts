import Enemy from "./enemy";

export default class BaseEnemy implements Enemy {
    takeDamage(): number {
        return 10;
    }

}