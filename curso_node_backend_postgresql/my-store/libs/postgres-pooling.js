const { Pool } = require('pg');
const { config } = require('../config/config');

const DB_DIALECT = encodeURIComponent(config.dbDialect);
let uri;

if (config.isProduction) {
    uri = config.dbUrl
} else {
    const USER = encodeURIComponent(config.dbUser);
    const PASS = encodeURIComponent(config.dbPassword);
    uri = `${DB_DIALECT}://${USER}:${PASS}@${config.dbHost}:${config.dbPort}/${config.dbName}`;
}

const pool = new Pool({
    connectionString: uri
});

module.exports = pool;