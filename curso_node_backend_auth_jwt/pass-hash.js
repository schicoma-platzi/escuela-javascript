const bcrypt = require('bcrypt');

async function hashPassword() {
    const myPassword = 'admin 123 .202';
    const hash = await bcrypt.hash(myPassword, 10); // $2b$10$mDlYGxV14iNW6QPkcAwuuurZ.ie8hjP/eKlqVpN/ueb7fnpMOp8jG
    console.log(hash);
}

hashPassword();
