interface Observer {
    update(data: any): void;
}

interface Subject {
    subscribe: (observer: Observer) => void;
    unsubscribe: (observer: Observer) => void;
}

class BitcoinPriceSubject implements Subject {
    observers: Observer[] = [];

    constructor() {
        const input: HTMLInputElement = document.querySelector('#value');
        input.addEventListener('input', (event: Event) => {
            this.notify(input.value);
        });
    }

    subscribe(observer: Observer) {
        this.observers.push(observer);
    };

    unsubscribe(observer: Observer) {
        const index = this.observers.findIndex(obs => {
            return obs === observer;
        });

        this.observers.splice(index, 1);
    };

    notify(data: any) {
        this.observers.forEach(observer => {
            observer.update(data);
        });
    }

}

class PriceDisplayObserver implements Observer {

    private element: HTMLElement;

    constructor() {
        this.element = document.querySelector('#price');
    }

    update(data: any): void {
        this.element.innerText = `$ ${data}`;
    }

}

// crear las instancias

const value = new BitcoinPriceSubject();
const display = new PriceDisplayObserver();

value.subscribe(display);

setTimeout(() => {
    value.unsubscribe(display);
}, 5000);