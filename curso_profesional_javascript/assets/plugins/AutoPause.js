/**
 * Usamos IntersectionObserver para observar elementos del DOM.
 */
class AutoPause {
    constructor() {
        this.threshold = 0.25; // umbral visible

        // el this en la funciónh handlerIntersection hará referencia al objeto IntersectionObserver
        // para que el this haga referencia a AutoPause, se usará la función bind.
        this.handlerIntersection = this.handlerIntersection.bind(this);

        this.handlerVisiblityChange = this.handlerVisiblityChange.bind(this);
    }

    run(player) {
        this.player = player;

        const observer = new IntersectionObserver(this.handlerIntersection, {
            threshold: this.threshold
        });

        observer.observe(player.media);

        /**
         * API Visiblility Change
         * 
         * Un sitio tiene un carrusel de imágenes que no debería avanzar a la siguiente diapositiva a no ser que el usuario esté viendo la página.
         * Una aplicación que muestra un panel de información y no se quiere que se actualice la información del servidor cuando la página no está visible.
         * Una página quiere detectar cuando se está precargando para poder mantener un recuento preciso de las vistas de página.
         * Un sitio desea desactivar los sonidos cuando el dispositivo está en modo de espera (el usuario presiona el botón de encendido para apagar la pantalla).
         */

        document.addEventListener('visibilitychange', this.handlerVisiblityChange);
    }

    handlerIntersection(entries) {
        const entry = entries[0];
        const isVisible = entry.intersectionRatio >= this.threshold;

        if (isVisible) {
            this.player.play();
        } else {
            this.player.pause();
        }
    }

    handlerVisiblityChange() {
        const isVisible = document.visibilityState === 'visible'; // 'visible' or 'hidden'

        if (isVisible) {
            this.player.play();
        } else {
            this.player.pause();
        }
    }
}

export default AutoPause;