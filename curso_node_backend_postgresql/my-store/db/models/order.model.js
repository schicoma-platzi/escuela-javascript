const { Model, DataTypes, Sequelize } = require("sequelize");
const { CUSTOMER_TABLE } = require("./customer.model");

const ORDER_TABLE = 'orders';

const OrderSchema = {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    customerId: {
        field: 'customer_id',
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
            model: CUSTOMER_TABLE,
            key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
    },
    createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: 'created_at',
        defaultValue: Sequelize.NOW
    },
    total: {
        type: DataTypes.VIRTUAL,
        get() {
            let sum = 0;
            if (this.products.length) {
                sum = this.products.reduce((acum, current) => {
                    console.log(JSON.stringify(current));
                    return (current.price * current.OrderProduct.amount) + acum
                }, 0);
            }
            return sum;
        }
    }
}

class Order extends Model {
    static associate(models) {
        this.belongsTo(models.Customer, {
            as: 'customer'
        });

        this.belongsToMany(models.Product, {
            as: 'products',
            through: models.OrderProduct,
            foreignKey: 'orderId',
            otherKey: 'productId'
        });
    }

    static config(sequelize) {
        return {
            sequelize,
            tableName: ORDER_TABLE,
            modelName: 'Order',
            timestamps: false
        }
    }
}

module.exports = { Order, ORDER_TABLE, OrderSchema };