const bcrypt = require('bcrypt');

async function verifyPassword() {
    const hash = '$2b$10$mDlYGxV14iNW6QPkcAwuuurZ.ie8hjP/eKlqVpN/ueb7fnpMOp8jG';

    const myPassword = 'admin 123 .202';
    const isMatch = await bcrypt.compare(myPassword, hash); // Promise<boolean>
    console.log(isMatch);

    const myOtherPassword = 'admin123';
    const noMatch = await bcrypt.compare(myOtherPassword, hash); // Promise<boolean>
    console.log(noMatch);
}

verifyPassword();
