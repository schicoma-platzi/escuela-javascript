# WebPack

Webpack es un module bundler. Herramienta que permite **preparar y optimizar** los recursos de nuestro proyecto para que pueda ser enviado a producción. Tiene muchas características como gestión de tareas, carga de módulos, preparación de imágenes, archivos estáticos y código fuente, entre otros. 

## Principios básicos

- Páquete de módulos de estáticos para apps de JS modernas.
- Realiza una construcción de gráficos de dependencias, con un punto de entrada y salida.
- Cuenta con loader y plugins especiales para añadir configuraciones especiales.

## Instalando Webpack

```npm install webpack webpack-cli -D```

## Ejecutando Webpack

### Desarrollo

Agrega código necesario para incluir webpack en la ejecución del código.

```npx webpack --mode development```

### Producción

Optimiza el código para enviarlo a producción

```npx webpack --mode production```

## Librerias utilizadas

- 'html-webpack-plugin'
- 'mini-css-extract-plugin'
- 'copy-webpack-plugin'
- 'css-minimizer-webpack-plugin'
- 'terser-webpack-plugin'
- 'dotenv-webpack'
- 'clean-webpack-plugin'

## Analizar recursos

Ejecutar el comando ```npx webpack --profile --json > stats.json```. Luego ejecutar ```npx webpack-bundle-analyzer stats.json```

## Loaders vs Plugins

Los loaders son módulos independientes que permiten a Webpack procesar diferentes tipos de archivos que no soporta de fábrica (Webpack solo es capaz de procesar archivos con extensión ```.js``` y ```.json```). Los recursos que serán manejados por los loaders son los que son importados con ```require()``` / ```import```.

Por otra parte, los plugins permiten realizar tareas de optimización, administración de assets e inyección de variables de entorno. Los plugins funcionan luego de que los loaders terminen de procesar y generar los bundles respectivos.

## Otros links

- https://google-webfonts-helper.herokuapp.com/fonts/ubuntu?subsets=latin