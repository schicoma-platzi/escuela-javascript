/**
 * Service workers
 * 
 * Sirven para hacer que nuestras aplicaciones funcionen Offline.
 * Muy usados en las Progressive Web Apps (PWA) los ServiceWorkers son una capa que vive entre el navegador y el Internet.
 * Parecido a como lo hacen los proxys van a interceptar peticiones para guardar el resultado en cache y la próxima vez que
 * se haga la petición tomar del cache ese resultado.
 * 
 */
const VERSION = 'v1';

self.addEventListener('install', event => {
    event.waitUntil(precache());
});

self.addEventListener('fetch', event => {
    const request = event.request;

    if (request.method !== 'GET') {
        return;
    }

    // get - buscar en caché
    event.respondWith(cacheResponse(request));

    // actualizar el cache
    event.waitUntil(updateCache(request));

});

async function precache() {
    const cache = await caches.open(VERSION);
    return cache.addAll([
        // '/',
        // '/index.html',
        // '/assets/index.js',
        // '/assets/MediaPlayer.js',
        // '/assets/plugins/AutoPlay.js',
        // '/assets/plugins/AutoPause.js',
        // '/assets/index.css',
        // '/assets/BigBuckBunny.mp4',
    ]);
}

async function cacheResponse(request) {
    const cache = await caches.open(VERSION);
    const response = await cache.match(request);

    return response || fetch(request); // en caso la respuesta no esté en cache, llamar al recurso con 'fetch'
}

async function updateCache(request) {
    const cache = await caches.open(VERSION);
    const response = await fetch(request);

    // cache.put no soporta carga parcial (206 status)
    return response.status === 200 ? cache.put(request, response) : new Promise((resolve, reject) => resolve());
}