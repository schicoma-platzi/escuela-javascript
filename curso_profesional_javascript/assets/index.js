import MediaPlayer from './MediaPlayer.js';
import AutoPlay from './plugins/AutoPlay.js';
import AutoPause from './plugins/AutoPause.js';
import AdsPlugin from './plugins/Ads/index';

// const button = document.querySelector('button');
const playButton = document.getElementById('playButton');
const muteButton = document.getElementById('muteButton');

const video = document.querySelector('video.movie');
const player = new MediaPlayer({
    el: video,
    plugins: [
        new AutoPlay(),
        new AutoPause(),
        new AdsPlugin()
    ]
});

playButton.onclick = () => {
    if (player.media.paused) {
        player.play();
    } else {
        player.pause();
    }
};

muteButton.onclick = () => {
    if (player.media.muted) {
        player.unmute();
    } else {
        player.mute();
    }
};

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('../sw.js').catch(error => {
        console.log(error.message);
    });
}