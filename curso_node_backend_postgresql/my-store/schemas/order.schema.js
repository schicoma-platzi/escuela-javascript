const Joi = require("@hapi/joi");

const id = Joi.number().integer();
const amount = Joi.number().integer().min(1);
const customerId = Joi.number().integer();

const getOrderSchema = Joi.object({
    id: id.required(),
});

const createOrderSchema = Joi.object({
    customerId: customerId.required()
});

const addItemSchema = Joi.object({
    orderId: id.required(),
    productId: id.required(),
    amount: amount.required()
});

module.exports = { getOrderSchema, createOrderSchema, addItemSchema };