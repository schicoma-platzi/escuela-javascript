
const { config } = require('../config/config');

const DB_DIALECT = encodeURIComponent(config.dbDialect);
let uri;


if (config.isProduction) {
    uri = config.dbUrl;
} else {
    const USER = encodeURIComponent(config.dbUser);
    const PASS = encodeURIComponent(config.dbPassword);
    uri = `${DB_DIALECT}://${USER}:${PASS}@${config.dbHost}:${config.dbPort}/${config.dbName}`
}

module.exports = {
    development: {
        url: uri,
        dialect: DB_DIALECT
    },
    production: {
        url: uri,
        dialect: DB_DIALECT,
        dialectOptions: {
            ssl: {
                rejectUnauthorized: false
            }
        }
    }
}