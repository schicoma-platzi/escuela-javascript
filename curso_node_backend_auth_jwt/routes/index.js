const express = require('express');
const passport = require('passport');
const { checkAdminRole, checkRoles } = require('../middlewares/auth.handler');

const authRouter = require('./auth.router');
const categoriesRouter = require('./categories.router');
const customersRouter = require('./customers.router');
const orderRouter = require('./orders.router');
const profileRouter = require('./profile.router');
const productsRouter = require('./products.router');
const usersRouter = require('./users.router');

function routerApi(app) {
  const router = express.Router();

  app.use('/api/v1', router);

  router.use('/products', passport.authenticate('jwt', { session: false }), checkRoles('admin', 'vendor'), productsRouter);
  router.use('/categories', passport.authenticate('jwt', { session: false }), checkAdminRole, categoriesRouter);
  router.use('/users', usersRouter);
  router.use('/orders', orderRouter);
  router.use('/profile', passport.authenticate('jwt', { session: false }), profileRouter);
  router.use('/customers', customersRouter);
  router.use('/auth', authRouter);
}

module.exports = routerApi;
