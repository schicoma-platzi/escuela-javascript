// Middlewares de error: tienen 4 argumentos, el primero es el error

const boom = require("@hapi/boom");
const { ValidationError } = require('sequelize')

function logErrors(error, request, response, next) {
    console.error('Sucedió un error', error);
    next(error);
}


function sequelizeErrorHandler(error, request, response, next) {
    if (!error.isBoom && error instanceof ValidationError) {
        const { output } = boom.internal(); // 500 no envía mensajes de error al cliente
        output.payload.message = error.errors[0].message;
        return response.status(output.statusCode).json(output);
    }

    next(error);
}

function boomErrorHandler(error, request, response, next) {
    if (error.isBoom) {
        const { output } = error;
        return response.status(output.statusCode).json(output);
    }

    next(error);
}

function errorHandler(error, request, response, next) {
    response.status(500).json({
        message: error.message,
        stack: error.stack
    });
}

module.exports = {
    logErrors,
    errorHandler,
    boomErrorHandler,
    sequelizeErrorHandler
};