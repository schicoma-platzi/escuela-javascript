function AutoPlay() {

}

AutoPlay.prototype.run = function (player) {
    if (!player.muted) { // muted no es una función, es un 'atributo virtual' (cuando trabajamos setters y getters).
        player.muted = true;
    }

    player.play();
};

export default AutoPlay;