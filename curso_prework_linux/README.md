# Curso de  Prework: Configuración de Entorno de Desarrollo en Linux

Para este curso trabajaremos con la distribución de Linux llamada Ubuntu.

## Instalación

### Con VirtualBox

Se descargará una imagen (.ISO) de Ubuntu de la web principal

- [Virtual Box](https://www.virtualbox.org/wiki/Downloads)
- [Ubuntu](https://ubuntu.com/)

## Con USB booteable

Con una computadora con Windows, instalar Rufus para crear un usb booteable. Este usb debe estar vacío para usarlo como instalador.

- [Rufus](https://rufus.ie/es/)

Ingresar a la BIOS (F2, F10, Del) de la máquina donde querramos instalar el SO. Luego configurar nuestro device como **dispositivo de arranque**.
Al reiniciar la máquina, el programa de instalación de Ubuntu se ejecutará.

Investigar como hacer *Dual Boot* para tener instalado dos SOs (comúnmente Windows y Linux) en una sola máquina.

## Herramientas web y editor de texto

La mayoría de instaladores para Ubuntu son archivos con extensiones .deb.

### Navegador

Software que nos permite interactuar con internet, accediendo a páginas construidas con HTML, CSS y JavaScript. Existen varios navegadores como Chrome, Firefox, Safari, etc.

- [Google Chrome](https://www.google.com/intl/es-419/chrome/) cuenta con varias versiones para desarrolladores como: Canary (últimos features en fase experimental), Dev y Stable (versión estable utilizada por defecto). Creado y soportado por Google

### Editor de texto y código

- [Visual Studio Code](https://code.visualstudio.com/) es el editor de código más utilizado para desarrollo de software. Creado y soportado por Microsoft, cuenta con plugins y extensiones para hacer más *poderoso* nuestro editor.

### Extensiones para VSCode

- **Prettier**: Permite dar formato a nuestro código fuente para que sea más legible de manera automática.
- **Live Server**: Permite levantar un servidor ultra ligero para previsualizar proyectos de desarrollo web. Cuenta con *hot reloading*.
- **Path Intellisense**: Habilita el autocompletado de tags, rutas, etc.
- **Material Icon Theme**: Personalización. Agrega íconos a nuestro editor.

Además, se pueden instalar temas (como Atom Dark Theme, Github Theme, etc) para personalizar nuestro editor.

## Terminal

Gracias a la utilidad ```apt``` podemos instalar software de manera sencilla desde la terminal de Ubuntu.

Por lo general, para el desarrollo web utilizamos Node.js. Podemos descargar distintas versiones de este software utilizando la utilidad ```nvm``` (Node Version Manager).

## Git y Github

Git es un sistema de control de versiones descentralizado que nos permite gestionar las versiones de nuestro proyecto. Github es una plataforma que permite guardar en repositorios el codigo de nuestros proyectos (basado en Git).

