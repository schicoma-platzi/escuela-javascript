import Singleton from './singleton';

// const singleton = new Singleton(); constructor privado
const singleton = Singleton.getInstance();
const sameSingleton = Singleton.getInstance();

console.log(singleton === sameSingleton); // true