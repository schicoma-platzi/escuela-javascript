function MediaPlayer(config) {
    this.media = config.el;
    this.plugins = config.plugins || [];

    this._initPlayer();
    this._initPlugins();
}

MediaPlayer.prototype._initPlugins = function () {
    const player = {
        play: () => this.play(),
        pause: () => this.pause(),
        media: this.media,
        get muted() {
            return this.media.muted;
        },
        set muted(value) {
            this.media.muted = value;
        }
    };

    this.plugins.forEach(element => {
        // element.run(this); // this es la instancia de MediaPlayer
        element.run(player);
    });
};

MediaPlayer.prototype._initPlayer = function () {
    this.container = document.createElement('div');
    this.container.style.position = 'relative';
    this.media.parentNode.insertBefore(this.container, this.media); // colocar el nuevo elemento antes de media
    this.container.appendChild(this.media);
}

MediaPlayer.prototype.play = function () {
    this.media.play();
};

MediaPlayer.prototype.pause = function () {
    this.media.pause();
};

MediaPlayer.prototype.mute = function () {
    this.media.muted = true;
};

MediaPlayer.prototype.unmute = function () {
    this.media.muted = false;
};

export default MediaPlayer;