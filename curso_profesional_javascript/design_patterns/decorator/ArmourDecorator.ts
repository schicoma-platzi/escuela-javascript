import EnemyDecorator from "./EnemyDecorator";

class ArmourDecorator extends EnemyDecorator {
    takeDamage(): number {
        return this.enemy.takeDamage() / 1.5;
    }
}

export default ArmourDecorator;