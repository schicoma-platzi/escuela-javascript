# Curso profesional de JavaScript

## Carga de scripts en el navegador

Todo script que carguemos en nuestra página tiene un llamado y una ejecución.

Tanto con async como defer podemos hacer llamados asíncronos pero tiene sus diferencias:

- async. Con async podemos hacer la petición de forma asíncrona y no vamos a detener la carga del DOM hasta que se haga la ejecución del código.
- defer. La petición es igual asíncrona como en el async pero va a deferir la ejecución del Javascript hasta el final de que se cargue todo el documento.

## Module scope

Al importar un script con JS, todas las variables globadas declaradas podrán ser accedidas desde la consola del navegador

![Alt text](screenshots/Screenshot-1.png "Title")

Al utilizar el atributo ```type="module"```, se activará el alcance a nivel de módulo. De esta manera, se hace inaccesible a dichas variables del archivo cargado.

![Alt text](screenshots/Screenshot-2.png "Title")

## IIFE

Inmediately invoked function expression o Expresión de función inmediantamente invocada. Es otra forma de ejecutar bloques de código con variables privadas utilizando ámbito de función. Desde ES6, el uso de ```let``` y ```const``` ***reemplaza*** este comportamiento

## Más conceptos generales

### Closures

Funciones especiales que retornar funciones (u objeto con funciones) capaces de recordar el ámbito en el que fue declarado. Esto hace que pueda usar variables declaradas fuera de su ámbito de ejecución. Por ejemplo:

```javascript
function counter() {
    let i = 0;
    
    return function () {
        console.log(++i);
    }
}

let myCounter = counter();
myCounter() // 1 
myCounter() // 2 
```

### this

En JavaScript, el uso de ```this``` puede tener diferentes comportamientos dependiendo el ámbito donde se ejecute.

#### Global

```javascript
console.log(this); // [object window]
```
#### Función

```javascript
function whoIsThis() {
    return this;
}

console.log(whoIsThis()); // [object window]

function whoIsThisStrict() {
    'use strict';
    return this;
}

console.log(whoIsThis()); // undefined
```

Valor asignado por default al ejecutar una función simple. Al usar el modo estricto (```"use strict"```), el valor retornado será ```undefined```.

#### Contexto de un objeto

```javascript
const person = {
    name: 'Sebastian',
    saludar: function () {
        console.log(`Hola, soy ${this.name}`);
    }
};

console.log(person.saludar()); // 'Hola, soy Sebastian'

const accion = person.saludar;
accion() // 'Hola, soy '
```
En este caso, ```this``` se refiere a la instancia del objeto que ejecuta la función. Si ejecutamos la función fuera del contexto del objeto, ```this``` tomará el valor por default.

#### Contexto de una 'clase'

```javascript
function Person(name) {
    this.name = name;
}

Person.prototype.saludar = function() {
    console.log(`Me llamo ${this.name}`);
};

const person = new Person('Sebastian');
person.saludar(); // Me llamo Sebastian
```

En este caso, ```this``` hace referencia al **objeto instanciado**, NO sobre el objeto prototipo.

## Uso de call, apply y bind

Métodos para invocar funciones. Las diferencias se muestran a continuación

### Establecer 'this' usando 'call'
```javascript
function saludar() {
    console.log(`Hola. Soy ${this.name} ${this.lastname}`);
}

const person = {
    name: 'Sebastian',
    lastname: 'Chicoma Sandmann'
};

saludar.call(person); // 'Hola. Soy Sebastian Chicoma Sandmann'
```

### Establecer 'this' usando 'call', pasando parámetros a la función

```javascript
function caminar(metros, direccion) {
    console.log(`${this.name} camina ${metros} metros hacia ${direccion}`);
}

caminar(); // ' camina undefined metros hacia undefined'
caminar.call(person); // 'Sebastian camina undefined metros hacia undefined'
caminar.call(person, 400, 'el norte'); // 'Sebastian camina 400 metros hacia el norte'
```

### Establecer 'this' usando 'apply', pasando parámetros a la función

```javascript
const array = [800, 'el sur'];
caminar.apply(person, array); // 'Sebastian camina 800 metros hacia el sur '
```

Para recordar la diferencia entre estos dós métodos, la siguiente técnica mnemotécnica es de mucha ayuda:

- call: c de commas
- apply: a de array

### Establecer 'this' en una función usando 'bind'

```javascript
const anotherPerson = {
    name: 'Gianluca',
    lastname: 'Lapadula'
};

const saludoDeAnotherPerson = saludar.bind(anotherPerson);
saludoDeAnotherPerson(); // 'Hola. Soy Gianluca Lapadula'

const anotherPersonCamina = caminar.bind(anotherPerson);
anotherPersonCamina(); // 'Gianluca camina undefined metros hacia undefined'
anotherPersonCamina(1000, 'el este'); // 'Gianluca camina 1000 metros hacia el este'
```

## Prototype

Es un mecanismo que utiliza JavaScript para emular el comportamiento de herencia de Programación orientada a objetos (JS es un lenguaje orientado a prototipos).

En este lenguaje no existe el concepto de clases (hasta antes de ES6). Por ello, se usa lo que se conoce como **prototype chain** para alcanzar dicha característica

Todas las funciones creadas tienen el objeto prototipal consigo.

```javascript
function Pilot(name, eva) {
    const pilot = Object.create(Pilot.prototype);
    pilot.nombre = name;
    pilot.eva = eva;

    return pilot;
}
```

Al usar ```Object.create()```, este crea un objeto nuevo (sin atributos ni funciones). Sin embargo, tiene un atributo interno llamado ```__proto__``` (puede variar dependiendo del entorno) con los atributos y las funcionalidades heredadas de la función pasada como parámetro


```javascript
Pilot.prototype.pilotar = function () {
    console.log(`El piloto ${this.nombre} activó el ${this.eva}`);
};
```

Además, este mecanismo permite agregar funciones y atributos de manera externa.

```javascript
const thirdChildren = Pilot('Shinji', 'EVA-01');
thirdChildren.pilotar();
```

### New

El keyword ```new``` es un sugar syntax que permite usar este mecanismo de manera implícita.

```javascript
function Pilot(name, eva) {
    // const pilot = Object.create(Pilot.prototype); 
    // this = pilot;
    this.nombre = name;
    this.eva = eva;

    // return this; // el valor se retorna implícitamente.
}

const thirdChildren = new Pilot('Shinji', 'EVA-01');
```

## Callback Queue vs. Microtask Queue
El microtask queue es una cola de mayor prioridad que el Callback queue. Aquí se guardarán las rutinas asíncronas relacionadas a Promises.

## Service workers
* Sirven para hacer que nuestras aplicaciones funcionen Offline.
* Muy usados en las Progressive Web Apps (PWA) los ServiceWorkers son una capa que vive entre el navegador y el Internet.
* Parecido a como lo hacen los proxys van a interceptar peticiones para guardar el resultado en cache y la próxima vez que
* se haga la petición tomar del cache ese resultado.

## Patrones de diseño

Recetas que permiten solucionar problemas en un determinado contexto. Existen tres tipos de patrones de diseño: Creacionales, Estructurales y De comportamiento

### Singleton

Patrón que asegura la creación de una sola instancia de una clase. Dicha instancia puede ser consumida por cualquier objeto.

### Observer

Patrón que define una dependencia de uno a muchos. De esta manera, cuando el objeto principal presenta un cambio, este notifica a todos los dependientes.
Es también conocido como patrón de publicación-suscripción.

El patrón observer se compone de un sujeto que ofrece mecanismos de suscripción y desuscripción a múltiples observadores que quieren ser notificados de los cambios en dicho sujeto. Cada observador expone un método de update que es usado por el sujeto para notificar cualquier cambio a todos los suscritos.

### Decorator

El patrón decorator responde a la necesidad de añadir dinámicamente diversas funcionalidades a un objeto, sin recurrir directamente a la herencia.

Composición vs Herencia