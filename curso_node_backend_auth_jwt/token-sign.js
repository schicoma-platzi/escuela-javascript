const jwt = require('jsonwebtoken');
const secret = 'my-secret-key'; // solo el backend debe saber este código, como env. var.
const payload = {
    sub: 1,
    // ... se puede agregar los valores que querramos
    role: 'customer'
}

function signToken(payload, secret) {
    return jwt.sign(payload, secret);
}

const token = signToken(payload, secret);
console.log(token);