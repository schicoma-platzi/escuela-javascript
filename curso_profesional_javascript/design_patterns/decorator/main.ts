import BaseEnemy from "./BaseEnemy";
import Enemy from "./enemy";
import HelmetDecorator from "./HelmetDecorator";
import ArmourDecorator from "./ArmourDecorator";

let enemy: Enemy = new BaseEnemy();

enemy = new ArmourDecorator(enemy);
enemy = new HelmetDecorator(enemy);

const damage = enemy.takeDamage();
console.log(`The enemy's damage is ${damage}`);