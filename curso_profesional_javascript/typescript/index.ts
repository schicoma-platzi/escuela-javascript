// introducción

console.log('Hello, TypeScript');

function add(a: number, b: number) {
    return a + b;
}

console.log(add(1, 2));

// tipos básicos

let muted = true;
muted = false;

// muted = 5; Type 'number' is not assignable to type 'boolean'.

// Números

let numerador = 42;
let denominador = 7;

let resultado: number = numerador / denominador;

// texto

let cadena: string = 'Hello world';
let otraCadena: string = `${cadena} significa Hola mundo`;

// arreglos

let people: string[] = [];
people = ['Sebastian', 'Jose', 'Leonardo'];
// people.push(900); Argument of type 'number' is not assignable to parameter of type 'string'

let peopleAndNumbers: Array<string | number> = [];
peopleAndNumbers = ['Sebastian', 'Jose', 'Leonardo'];
peopleAndNumbers.push(900);

// enum

enum Color {
    Rojo = 'Rojo',
    Verde = 'Verde',
    Azul = 'Azul'
};

let colorFavorito: Color = Color.Rojo;
console.log(`Mi color favorito es ${colorFavorito}`);

// any

let comodin: any = 'Joker';
comodin = { joker: comodin };

/**
 * Funciones
 * 
 */

function multiply(a: number, b: number): number {
    return a * b;
}

const product = multiply(5, 4);

function createMultipler(a: number): (number) => number { // valor de retorno: funcion (parametro numerico) que retorna un numero
    return function (
        b: number) {
        return a * b;
    }
}

const multiplierFive = createMultipler(5);
const product30 = multiplierFive(6);
console.log(product30);

function fullName(firstname: string, lastname?: string): string {
    return `${firstname} ${lastname}`;
}

function fullNameDefaultValue(firstname: string, lastname: string = 'Chicoma'): string {
    return `${firstname} ${lastname}`;
}

const person = fullName('Sebastian', 'Chicoma');
const person2 = fullName('Drake');
const person3 = fullNameDefaultValue('Fernando');

console.log(person, person2, person3);

/**
 * Interfaces
 * 
 * En POO, es un contrato o un conjunto de métodos que permiten determinar el funcionamiento de una clase (molde o plantilla para su definición)
 */

interface Rectangulo {
    ancho: number;
    alto: number;
    color: Color;
}

let rect: Rectangulo = {
    ancho: 6,
    alto: 3,
    color: Color.Verde
};

rect.toString = function (): string {
    return `Un rectángulo ${this.color}`;
}

function area(r: Rectangulo): number {
    return r.alto * r.ancho;
}

console.log(area(rect));
console.log(rect.toString());
