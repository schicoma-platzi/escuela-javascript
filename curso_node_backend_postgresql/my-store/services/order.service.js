const boom = require('@hapi/boom');
const { Op } = require('sequelize');

const { models } = require('./../libs/sequelize');

class OrderService {

    constructor() {
    }

    async create(data) {
        const newOrder = await models.Order.create(data);
        return newOrder;
    }

    async find() {
        return [];
    }

    async findOne(id) {
        const order = await models.Order.findByPk(id, {
            // include: ['customer']
            include: [
                {
                    association: 'customer',
                    include: ['user']
                },
                'products'
            ]
        });

        if (!order) {
            throw boom.notFound('order not found');
        }
        return order;
    }

    async addItem(data) {
        const item = await models.OrderProduct.findOne({
            where: {
                orderId: {
                    [Op.eq]: data.orderId
                },
                productId: {
                    [Op.eq]: data.productId
                }
            }
        });

        let newItem;

        if (item) {
            newItem = await item.update(data);
        } else {
            newItem = await models.OrderProduct.create(data);
        }

        return newItem;
    }

    async update(id, changes) {
        return {
            id,
            changes,
        };
    }

    async delete(id) {
        return { id };
    }

}

module.exports = OrderService;