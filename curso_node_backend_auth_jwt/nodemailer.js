const nodemailer = require("nodemailer");
const { config } = require("./config/config");

// async..await is not allowed in global scope, must use a wrapper
async function sendMail() {
    console.log('// create reusable transporter object using the default SMTP transport');
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: config.emailUser,
            pass: config.emailPass
        }
    });

    console.log('// Send mail with defined transport object');
    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: '"Fred Foo 👻" <foo@example.com>', // sender address
        // to: "bar@example.com, baz@example.com", // list of receivers
        to: "sebastian.chicoma@globant.com",
        subject: "Hello ✔", // Subject line
        text: "Hello world?", // plain text body
        html: "<b>Hello world?</b>", // html body
    });

    console.log("Message sent: %s", info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

    // Preview only available when sending through an Ethereal account
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

sendMail().catch(console.error);