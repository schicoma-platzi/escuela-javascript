const bcrypt = require('bcrypt');
const boom = require("@hapi/boom");
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const UserService = require('./user.service');

const { config } = require('../config/config');

const userService = new UserService();

class AuthService {
    async getUser(email, password) {
        const user = await userService.findOneByEmail(email);

        // validar si usuario existe
        if (!user) {
            throw boom.unauthorized();
        }

        // validar si la contraseña es correcta
        const isMatch = await bcrypt.compare(password, user.password);

        if (!isMatch) {
            throw boom.unauthorized();
        }

        delete user.dataValues.password;

        return user;
    }

    signToken(user) {
        const payload = {
            sub: user.id,
            role: user.role
        };

        const token = jwt.sign(payload, config.jwtSecret);
        return token;
    }

    async changePassword(token, newPassword) {
        try {
            const payload = jwt.verify(token, config.jwtSecret);
            const user = await userService.findOne(payload.sub);

            if (user.recoveryToken !== token) {
                throw new Error('invalid token');
            }
            const hashedPassword = await bcrypt.hash(newPassword, 10);

            await userService.update(user.id, { recoveryToken: null, password: hashedPassword });
        } catch (error) {
            throw boom.unauthorized(error.message);
        }
    }

    async sendRecoveryPasswordEmail(email) {
        const user = await userService.findOneByEmail(email);

        if (!user) {
            throw boom.internal();
        }

        // generar token con firma jwt
        const payload = { sub: user.id };
        const token = jwt.sign(payload, config.jwtSecret, { expiresIn: '5min' }); // podemos tener otro secret para un nuevo proceso (recovery password)

        const link = `http://localhost:3131/recovery?token=${token}`;

        // insertar el token en la base de datos
        await userService.update(user.id, { recoveryToken: token });

        // enviar el correo
        const infoMail = {
            from: `"Sebastian Chicoma" <${config.emailUser}>`,
            to: email,
            subject: "Recuperación de contraseña",
            html: `<b>Ingresa a este link para recuperar la contraseña: </b><a href="${link}">${link}</a>`
        };

        await this.sendMail(infoMail);
    }

    async sendMail(infoMail) {
        let transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: config.emailUser,
                pass: config.emailPass
            }
        });

        await transporter.sendMail(infoMail);
    }
}

module.exports = AuthService;