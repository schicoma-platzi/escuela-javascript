const express = require('express');
const passport = require('passport');
const AuthService = require('../services/auth.service');

const authService = new AuthService();
const router = express.Router();

router.post('/login',
    passport.authenticate('local', { session: false }),
    async (request, response, next) => {
        try {

            const user = request.user;
            const token = authService.signToken(user);

            response.json({
                user,
                token
            });

        } catch (error) {
            next(error);
        }
    }
);

router.post('/recovery',
    async (request, response, next) => {
        try {
            const { email } = request.body;
            await authService.sendRecoveryPasswordEmail(email);

            response.json({ message: 'email sent' });
        } catch (error) {
            next(error);
        }

    });

router.post('/change-password',
    async (request, response, next) => {
        try {
            const { token, password } = request.body;
            await authService.changePassword(token, password);

            response.json({ message: 'The password was changed' });
        } catch (error) {
            next(error);
        }
    });

module.exports = router;
