const faker = require('faker');
const boom = require('@hapi/boom');

// const pool = require('../libs/postgres-pooling');
const sequelize = require('../libs/sequelize');

class ProductService {

    constructor() {
        this.products = [];
        this.generate();

        // this.pool = pool;
        // this.pool.on('error', (err) => {
        //     console.error(err);
        // });
    }

    generate() {
        let limit = 50;
        for (let i = 0; i < (limit > 50 ? 50 : limit); i++) {
            this.products.push({
                id: faker.datatype.uuid(),
                name: faker.commerce.productName(),
                price: parseInt(faker.commerce.price(), 10),
                image: faker.image.imageUrl(),
                isBlock: faker.datatype.boolean()
            });
        }
    }

    async create(data) {
        const newProduct = {
            id: faker.datatype.uuid(),
            ...data
        }

        this.products.push(newProduct);

        return newProduct;
    }

    async find() {
        // node-postgres sql query
        // const query = 'SELECT * from task';
        // const result = await this.pool.query(query); //built-in way to get any client in the pool
        // return result.rows;

        // sequelize default query
        const query = 'SELECT * from task';
        const [data, metadata] = await sequelize.query(query);
        return data;
    }

    async findOne(id) {
        const product = this.products.find(item => item.id === id);
        if (!product) {
            throw boom.notFound('Product not found in the app');
        } else if (product.isBlock) {
            throw boom.conflict('Forbidden product');
        }

        return product;
    }

    async update(id, changes) {
        const index = this.products.findIndex(product => product.id === id);

        if (index === -1) {
            // throw new Error('Product not found'); // default error middleware
            throw boom.notFound('Product not found in the app');
        }

        const product = this.products[index];
        this.products[index] = {
            ...product,
            ...changes
        }

        return this.products[index];
    }

    async delete(id) {
        const index = this.products.findIndex(product => product.id === id);

        if (index === -1) {
            throw boom.notFound('Product not found in the app');
        }

        this.products.splice(index, 1);
        return { id };
    }

}

module.exports = ProductService