# Curso de Backend con Node.js: Autenticación con Passport.js y JWT

## Recomendaciones previas

```git subtree add --prefix=curso_node_backend_auth_jwt git@github.com:platzi/curso-nodejs-auth.git  main``` para descargar un proyecto previo en un repositorio existente.

## Autenticación vs Autorización

La autenticación es el proceso de verificación de *quíenes somos* de acuerdo a una serie de credenciales (usuario y contraseña comúnmente).
La autorización se encarga de la gestión de los permisos y privilegios de acuerdo a las credenciales con las que nos autenticamos.

## JWT

JSON Web Tokens. Técnica open-source de comprobación *stateless* de autenticación. Reemplaza al uso de cookies para manejo de sesiones. Permite soportar diferentes clientes para lograr crear un sistema distribuido

## Manejo de Auth desde el cliente

- Cookies sobre LocalStorage.
- Enviar el token en los headers del request (de acuerdo al tipo de autenticación).
- Usar interceptores para evitar duplicar el código para colocar los tokens en los headers. Frameworks como Angular permiten realizar esto de manera sencilla con el uso de *interceptors*.
- Utilizar Refresh Token para revalidar permisos.
- Validación de permisos con JWT.

## Passport

Passport es un *middleware* de Node.js para autenticación. Flexible y modular. Comprende un conjunto completos de **estrategias** de autenticación usando usuario y contraseña, Google, Facebook, Twitter y más.

## Links adicionales

- https://keygen.io/
- https://jwt.io/
- https://nodemailer.com/about/

## Deploy en Heroku

Ejecutar el comando para configurar, desde la terminal, variables de entorno con sus respectivos valores:
```heroku config:set API_KEY=<valor> JWT_SECRET=<valor> EMAIL_USER=<valor> EMAIL_PASS=<valor>```