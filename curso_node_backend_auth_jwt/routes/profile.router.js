const express = require('express');

const OrderService = require('../services/order.service');

const router = express.Router();
const service = new OrderService();

router.get(
    '/my-orders',
    async (request, response, next) => {
        try {
            // payload del token
            const { sub: userId } = request.user;

            const order = await service.findByUser(userId);
            response.json(order);
        } catch (error) {
            next(error);
        }
    }
);


module.exports = router;
